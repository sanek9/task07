#include <stdio.h>
#include <string.h>

int main(int args, char *argv[]){
	char s[100];
	printf("Input string:\n");
	gets(s);
	int i, g, str_len=strlen(s), mxrc=0, rc=0;
	char mxs[100]="";
	char buf[2];
	for(i=0;i<str_len;i++){
		//cs=s[i];
		rc=0;
		for(g=i;g<str_len;g++){
			if(s[i]==s[g]){
				rc++;
			}
		}
		if(mxrc<rc){
			mxrc=rc;
			mxs[0]=s[i];
			mxs[1]='\0';
		}else if(mxrc==rc){
			buf[0]=s[i];
			buf[1]='\0';
			strcat(mxs,buf);
		}
	}
	printf("symbol%c ",(strlen(mxs)>1)?'s':0);
	for(i=0;mxs[i]!='\0';i++)
		printf("'%c'%c ",mxs[i],(mxs[i+1]!='\0')?',':0);
	printf("%s repeated %d times\n",(strlen(mxs)>1)?"are":"is",mxrc);
	return 0;
}
