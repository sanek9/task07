#include <stdio.h>
#include <string.h>

int main(int args, char *argv[]){
	char s[100];
	char buf;
	printf("Input string:\n");
	gets(s);
	int i, g, k, l=0, b=0, len=strlen(s);
	for(i=0;i<=len;i++){
		if(isalnum(s[i])){
			if(!b){
				g=i;
				b=1;
				l=1;
			}else{
				l++;
			}
		}else if(l>1){
			b=0;
			for(k=0;k<l/2;k++){
				buf=s[g+k];
				s[g+k]=s[g+(l-k)-1];
				s[g+(l-k)-1]=buf;
			}
			l=0;
		}
	}
	printf("\n%s\n",s);
	return 0;
}
