#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int split(char **,char *);
int func(const void *, const void *);
void sort(char *);
void freelist(char **,int);

int main(int args, char *argv[]){

	int i,g,list_size=25;
	char *ls[25];
	char *str;
	printf("Input text:\n");
	for(i=0;i<list_size;i++){
		str=(char*)calloc(sizeof(char),80);
		gets(str);
		ls[i]=str;
		sort(str);
		if(strlen(str)==0)
			list_size=i+1;
	}

	printf("\nSorted Text:\n");


	for(i=0;i<list_size;i++){
		g=0;
		while(ls[i][g]!='\0'){
			putchar(ls[i][g]);
			g++;
		}
		putchar('\n');
        }

}


void sort(char *str){
	int s_bls=0;
	int i=0,g;
	char *bls[80/2];

	s_bls=split(bls,str);

	qsort(bls,s_bls,sizeof(char *),func);
	str[0]='\0';
	for(i=0;i<s_bls;i++){
		strcat(str,bls[i]);
		strcat(str," ");
	}
	freelist(bls,s_bls);
}

int func(const void *a, const void *b){
	return strcmp( (char*) (*(int*)(a)), (char*)(*(int*) (b)) );
}

void freelist(char **list,int size){
	int i;
	for(i=0;i<size;i++)
		free((void*)list[i]);

}
int split(char **ls,char *str){
	char *buf;
	char *i=str-1,*p;
	int g=0,k=0,b=0,l=0;
	do{
		i++;
		if(isalnum(*i)){
			if(!b){
				l=1;
				b=1;
				p=i;
			}else{
				l++;
			}
		}else if(l>0){
			buf=(char*)calloc(sizeof(char),l+1);
			strncpy(buf,p,l);
			ls[k]=buf;
			k++;
			b=0;
			l=0;
		}
	}while(*i!='\0');
	return k;
}
